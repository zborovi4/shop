/**
 * Created by OleksandrZborovskyi on 19.01.2022.
 */

import {LightningElement, track} from 'lwc';

export default class DeleteOrdersModal extends LightningElement {
    @track openModal = true;
    showModal() {
        this.openModal = true;
    }
    closeModal() {
        this.openModal = false;
    }
}